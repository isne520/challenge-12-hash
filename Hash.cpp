#include<iostream>
#include<string>
#include<list>

using namespace std;

struct student { //use struct command to group ID(integer) and name(string) under one name
	int ID; //declare an integer named ID
	string name; //declare a string called name
	student() { //set the default value of student
		ID = 0;
		name = "";
	}
}Data_Input;

list<student> ID_table[50]; //declare an array of linked list named ID_table
list<student> name_table[50]; //declare an array of linked list named name_table

int Hash_ID(int input) { //ID hashing function (function that is used to find the position in the array)
	int output;
	output = input % 50;
	return output; //return the position
}

int Hash_name(string input) { //name hashing function (function that is used to find the position in the array)
	int output = 0;
	for (int i = 0; i < input.length(); i++) {
		output = output + toupper(input.at(i));
	}
	return (output % 50); //return the position
}

void data_input() { //function for inputting data into the array
	student input_info;
	int ID; //declare an integer to store the ID that is going to be inputted
	string name; //declare a string to store the name that is going to be inputted
	cout << "Please enter student's ID you want to input: ";
	cin >> ID;
	cout << "Please enter student's name you want to input: ";
	cin >> name;

	input_info.ID = ID;
	input_info.name = name;

	ID_table[Hash_ID(ID)].push_back(input_info); //use hash function to find the position in the array and push the ID back
	name_table[Hash_name(name)].push_back(input_info); //use hash function to find the position in the array and push the name back
}

void table() { //function that is used to input the ID and name into the table

	Data_Input.ID = 600611001;
	Data_Input.name = "Same";
	ID_table[Hash_ID(Data_Input.ID)].push_back(Data_Input);
	name_table[Hash_name(Data_Input.name)].push_back(Data_Input);

	Data_Input.ID = 600611002;
	Data_Input.name = "Lukkaew";
	ID_table[Hash_ID(Data_Input.ID)].push_back(Data_Input);
	name_table[Hash_name(Data_Input.name)].push_back(Data_Input);

	Data_Input.ID = 600611003;
	Data_Input.name = "Pappap";
	ID_table[Hash_ID(Data_Input.ID)].push_back(Data_Input);
	name_table[Hash_name(Data_Input.name)].push_back(Data_Input);

	Data_Input.ID = 600611004;
	Data_Input.name = "Aum";
	ID_table[Hash_ID(Data_Input.ID)].push_back(Data_Input);
	name_table[Hash_name(Data_Input.name)].push_back(Data_Input);

	Data_Input.ID = 600611005;
	Data_Input.name = "Ohm";
	ID_table[Hash_ID(Data_Input.ID)].push_back(Data_Input);
	name_table[Hash_name(Data_Input.name)].push_back(Data_Input);

	Data_Input.ID = 600611006;
	Data_Input.name = "Dome";
	ID_table[Hash_ID(Data_Input.ID)].push_back(Data_Input);
	name_table[Hash_name(Data_Input.name)].push_back(Data_Input);

	Data_Input.ID = 600611007;
	Data_Input.name = "Chom";
	ID_table[Hash_ID(Data_Input.ID)].push_back(Data_Input);
	name_table[Hash_name(Data_Input.name)].push_back(Data_Input);

	Data_Input.ID = 600611008;
	Data_Input.name = "Garfield";
	ID_table[Hash_ID(Data_Input.ID)].push_back(Data_Input);
	name_table[Hash_name(Data_Input.name)].push_back(Data_Input);

	Data_Input.ID = 600611009;
	Data_Input.name = "Ice";
	ID_table[Hash_ID(Data_Input.ID)].push_back(Data_Input);
	name_table[Hash_name(Data_Input.name)].push_back(Data_Input);

	Data_Input.ID = 600611010;
	Data_Input.name = "Pon";
	ID_table[Hash_ID(Data_Input.ID)].push_back(Data_Input);
	name_table[Hash_name(Data_Input.name)].push_back(Data_Input);

	Data_Input.ID = 600611011;
	Data_Input.name = "Fluky";
	ID_table[Hash_ID(Data_Input.ID)].push_back(Data_Input);
	name_table[Hash_name(Data_Input.name)].push_back(Data_Input);

	Data_Input.ID = 600611012;
	Data_Input.name = "Blue";
	ID_table[Hash_ID(Data_Input.ID)].push_back(Data_Input);
	name_table[Hash_name(Data_Input.name)].push_back(Data_Input);

	Data_Input.ID = 600611013;
	Data_Input.name = "But";
	ID_table[Hash_ID(Data_Input.ID)].push_back(Data_Input);
	name_table[Hash_name(Data_Input.name)].push_back(Data_Input);

	Data_Input.ID = 600611014;
	Data_Input.name = "Leo";
	ID_table[Hash_ID(Data_Input.ID)].push_back(Data_Input);
	name_table[Hash_name(Data_Input.name)].push_back(Data_Input);

	Data_Input.ID = 600611015;
	Data_Input.name = "Book";
	ID_table[Hash_ID(Data_Input.ID)].push_back(Data_Input);
	name_table[Hash_name(Data_Input.name)].push_back(Data_Input);

	Data_Input.ID = 600611016;
	Data_Input.name = "Teetee";
	ID_table[Hash_ID(Data_Input.ID)].push_back(Data_Input);
	name_table[Hash_name(Data_Input.name)].push_back(Data_Input);

	Data_Input.ID = 600611017;
	Data_Input.name = "Plug";
	ID_table[Hash_ID(Data_Input.ID)].push_back(Data_Input);
	name_table[Hash_name(Data_Input.name)].push_back(Data_Input);

	Data_Input.ID = 600611018;
	Data_Input.name = "Mo";
	ID_table[Hash_ID(Data_Input.ID)].push_back(Data_Input);
	name_table[Hash_name(Data_Input.name)].push_back(Data_Input);

	Data_Input.ID = 600611019;
	Data_Input.name = "Toey";
	ID_table[Hash_ID(Data_Input.ID)].push_back(Data_Input);
	name_table[Hash_name(Data_Input.name)].push_back(Data_Input);

	Data_Input.ID = 600611020;
	Data_Input.name = "Bas";
	ID_table[Hash_ID(Data_Input.ID)].push_back(Data_Input);
	name_table[Hash_name(Data_Input.name)].push_back(Data_Input);
}

void ID_Search() { //function that is used to search for the student by inputting the ID
	int id; //declare an integer to stored an ID
	cout << "Please enter the student's ID you want to find: ";
	cin >> id;

	int found = 0; //declare an integer that is used to check whether the ID is found or not
	for (list<student>::iterator a = ID_table[Hash_ID(id)].begin(); a != ID_table[Hash_ID(id)].end(); a++) {
		if (a->ID == id) { //condition for ID is found in the array of linked list
			cout << "FOUND!!!" << endl;
			cout << a->ID << " " << a->name << endl;
			found = 1; //if ID is found, change found to 1
		}
	}
	if (found == 0) { //condition if ID is not found in the array of linked list
		cout << "Student's not found" << endl;
	}
}

bool name_check(string name1, string name2) { //function for checking whether the name are the same or not
	int n1 = name1.length(); //declare an array to stored the length of the first string
	int n2 = name2.length(); //declare an array to stored the length of the second string
	int count = 0; //declare an integer that is used to count the letter of the string
	if (n1 == n2) { //condition if the length of string name1 and name2 are the same
		for (int i = 0; i < name1.length(); i++) { //loop for checking each letters of 2 strings
			if (toupper(name1[i]) == toupper(name2[i])) { //condition that check each letter
				count++; //increase the value of count by 1 in order to count the letter in the string
			}
		}
		if (count == name1.length()) { //if the length of string name1 is as long as the value of count
			return true; //return true which mean both strings are the same
		}
		else { //condition if both strings are difference
			return false; //return false which mean both strings are difference
		}
	}
	else { //condition if the length of both strings are not equal
		return false; //return false which mean both strings are difference
	}
}

void name_Search() { //function that is used to search for the student by inputting the name
	string Name; //declare a string to stored a name
	cout << "Please enter the student's name you want to find: ";
	cin >> Name;

	int found = 0; //declare an integer that is used to check whether the name is found or not
	for (list<student>::iterator a = name_table[Hash_name(Name)].begin(); a != name_table[Hash_name(Name)].end(); a++) {
		if (name_check(a->name, Name)) { //condition for name is found in the array of linked list
			cout << "FOUND!!!" << endl;
			cout << a->ID << " " << a->name << endl;
			found = 1; //if ID is found, change found to 1
		}
	}
	if (found == 0) { //condition if ID is not found in the array of linked list
		cout << "Student's not found" << endl;
	}
}

void main() {

	table(); //used table function to declare an array of linked list

	//create a loop that will work until the user type -1
	int i = 0;
	while (i != -1) {
		cout << "Hello! This is student's searching program." << endl;
		cout << "For inputting student's data, please press 1" << endl;
		cout << "For searching student's data by ID , please press 2" << endl;
		cout << "For searching student's data by name , please press 3" << endl;
		cout << "For quit the program , please press any key aside from 1,2,3" << endl;

		cin >> i;

		if (i == 1) { //condition if the user input 1
			data_input();
			cout << endl;
		}
		else if (i == 2) { //condition if the user input 2
			ID_Search();
			cout << endl;
		}
		else if (i == 3) { //condition if the user input 3
			name_Search();
			cout << endl;
		}
		else { //condition if the user input -1
			i = -1;
		}
	}
	system("pause");
}